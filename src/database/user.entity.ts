import { Column, Entity, Index } from "typeorm";
import { Exclude } from "class-transformer";
import { BaseEntity } from "../general/base.entity";

@Entity({ name: 'users' })
export class User extends BaseEntity {

  @Column({ nullable: true })
  name: string;

  @Index({ unique: true })
  @Column({ nullable: false })
  email: string;

  @Exclude({ toPlainOnly: true })
  @Column({ nullable: false })
  password: string;

  @Column({ unique: true, nullable: true })
  mobile: string;

  @Column({ nullable: true })
  avatar: string;
}