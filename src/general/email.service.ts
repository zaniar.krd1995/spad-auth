import { MailerService } from "@nestjs-modules/mailer";
import { Injectable } from "@nestjs/common";
require('dotenv').config({path: `.env.${process.env.NODE_ENV}`});

@Injectable()
export class EmailService {
  
  constructor(private readonly mailerService: MailerService) {}

  async sendConfirmEmail(to: string, otp: string) {
    return this.mailerService.sendMail({
      subject: 'Confirm Email',
      text: `your registration OTP is ${otp}`,
      from: process.env.EMAIL_SENDER,
      to,
    });
  }

}