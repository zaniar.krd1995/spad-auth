import { CacheModule, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersRepository } from './database/user.repository';
import { TypeOrmExModule } from './general/typeorm-ex.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { EmailService } from './general/email.service';
import * as redisStore from 'cache-manager-redis-store';
require('dotenv').config({path: `.env.${process.env.NODE_ENV}`});

@Module({
  imports: [
    CacheModule.register({
      store: redisStore,
      host: process.env.REDIS_HOST,
      port: process.env.REDIS_PORT,
      name: process.env.REDIS_NAME,
      keyPrefix: process.env.REDIS_PREFIX,
      isGlobal: true
    }),

    MailerModule.forRoot({
      transport: {
        host: process.env.HOST,
        port: process.env.PORT,
        auth: { 
          user: process.env.USER,
          pass: process.env.PASSWORD,
        }
      }
    }),

    TypeOrmExModule.forCustomRepository([UsersRepository]),

    JwtModule.register({
      secret: process.env.SECRET,
      signOptions: { expiresIn: `${process.env.TOKEN_EXPIRATION_TIME}` }
    }),

    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      autoLoadEntities: true,
      migrationsRun: false,
      synchronize: false,
      entities: ['dist/src/**/*.entity.js'],
      migrations: ['dist/src/database/migrations/*.js'],
    }),
  ],

  controllers: [AppController],
  providers: [AppService, EmailService],
})
export class AppModule {}
