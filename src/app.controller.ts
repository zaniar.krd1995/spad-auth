import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { AppService } from './app.service';
import { IMessageResponse } from './dto/message-response.interface';
import { IRegisterDTO } from './dto/register.interface';
import { ISigninDTO } from './dto/signin.interface';
import { ISignupDTO } from './dto/signup.interface';
import { IToken } from './dto/token.interface';

// TODO Add forgot password

@Controller()
export class AppController {
  
  constructor(
    private readonly service: AppService,
  ) {}


  /**
   * Send OTP email and caching
   */
   @GrpcMethod('AuthService', 'Register')
  async register(dto: IRegisterDTO, metadata: any): Promise<IMessageResponse> {
    return {
      message: await this.service.register(dto)
    }
  }


  /**
   * Create user
   */
  @GrpcMethod('AuthService', 'Signup')
  async signup(dto: ISignupDTO, metadata: any): Promise<IToken> {
    return {
      token: await this.service.signup(dto)
    }    
  }


  @GrpcMethod('AuthService', 'Signin')
  async signin(dto: ISigninDTO, metadata: any): Promise<IToken> {
    return {
      token: await this.service.signin(dto)
    }
  }

}
