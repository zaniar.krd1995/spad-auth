import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { join } from 'path';
import { AppModule } from './app.module';

const authMicroserviceOptions = {
  transport: Transport.GRPC,
  options: {
    url: 'localhost:50052',
    package: 'auth',
    protoPath: join(__dirname, '../../src/auth.proto'),
  },
}

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, authMicroserviceOptions);
  await app.listen(); 
}
bootstrap();
