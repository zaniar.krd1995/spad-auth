import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersRepository } from './database/user.repository';
import { ISigninDTO } from './dto/signin.interface';
import { User } from './database/user.entity';
import { EmailService } from './general/email.service';
import { Cache } from "cache-manager";
import { IRegisterDTO } from './dto/register.interface';
import { ISignupDTO } from './dto/signup.interface';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AppService {

  constructor(
    private readonly repository: UsersRepository,
    private readonly jwtService: JwtService,
    private readonly emailService: EmailService,

    @Inject(CACHE_MANAGER) 
    private readonly cacheManager: Cache
  ) {}


  async register(dto: IRegisterDTO): Promise<string> {
    const user = await this.repository.findByEmail(dto.email);
    if (user) {
      console.log('throw grpc already exists exception');
      // TODO throw grpc already exists exception
    }

    const otp = Math.random().toString().substring(2, 8);
    const key = `${otp}`;
    const value = JSON.stringify(dto);

    const TWO_MINUTES = 120;
    await this.cacheManager.set(key, value, { ttl: TWO_MINUTES });
    this.emailService.sendConfirmEmail(dto.email, otp);
    
    return 'Email sent successfully';
  }


  async signup(dto: ISignupDTO): Promise<string> {
    const registerDTOStringify: string = await this.cacheManager.get(`${dto.otp}`);
    const registerDTO: IRegisterDTO = JSON.parse(registerDTOStringify);

    if (dto.email != registerDTO.email) {
      console.log('throw an grpc exception email not found');
      // TODO throw an grpc exception email not found
    }

    const hashedPassword = await this.hashPasswordWithSalt(registerDTO.password);

    const newUser = this.repository.create({
      email: registerDTO.email,
      password: hashedPassword
    });
    await this.repository.save(newUser);
    return this.sign(newUser.id);
  }


  private async hashPasswordWithSalt(password: string): Promise<string> {
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(password, salt);
    return hashedPassword;
  }


  async signin(dto: ISigninDTO) {
    const user = await this.repository.findByEmail(dto.email);
    if (!user) {
      // TODO throw grpc exception 
    };
    
    const isPasswordMatched = await bcrypt.compare(dto.password, user.password);
    if (!isPasswordMatched) {
      // TODO throw grpc exception
    }
    return this.sign(user.id);
  }


  private sign(id: string): string {
    return this.jwtService.sign({sub: id});
  }

}
