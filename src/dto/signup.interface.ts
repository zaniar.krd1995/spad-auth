export interface ISignupDTO {
  email: string;
  otp: string;
}